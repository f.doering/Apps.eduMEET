import type {
	IAppAccessors,
	IConfigurationExtend,
	IConfigurationModify,
	IEnvironmentRead,
	IHttp,
	ILogger,
	IRead,
} from '@rocket.chat/apps-engine/definition/accessors';
import { App } from '@rocket.chat/apps-engine/definition/App';
import type { IAppInfo } from '@rocket.chat/apps-engine/definition/metadata';
import type { ISetting } from '@rocket.chat/apps-engine/definition/settings';

import { AppSetting, settings } from './settings';
import { eduMEETProvider } from './videoConfProvider';
import { eduMEETSlashCommand } from './slashCommand';

export class eduMEETApp extends App {
	private provider: eduMEETProvider | undefined;

	public roomNamePrefix = '';
	public roomNameSuffix = '';

	public additionalParams: Record<string, any> = {};

	constructor(info: IAppInfo, logger: ILogger, accessors: IAppAccessors) {
		super(info, logger, accessors);
	}

	protected async extendConfiguration(configuration: IConfigurationExtend): Promise<void> {
		await Promise.all(settings.map((setting) => configuration.settings.provideSetting(setting)));
		await configuration.slashCommands.provideSlashCommand(new eduMEETSlashCommand(this));

		const provider = this.getProvider();
		await configuration.videoConfProviders.provideVideoConfProvider(provider);
	}

	public async onEnable(environmentRead: IEnvironmentRead, _configModify: IConfigurationModify): Promise<boolean> {
		const settings = environmentRead.getSettings();

		const provider = this.getProvider();

		provider.url = await settings.getValueById(AppSetting.Url);
		provider.roomNamePrefix = await settings.getValueById(AppSetting.roomNamePrefix);
		provider.roomNameSuffix = await settings.getValueById(AppSetting.roomNameSuffix);

		return true;
	}

	public async onSettingUpdated(setting: ISetting, _configModify: IConfigurationModify, _read: IRead, _http: IHttp): Promise<void> {
		const provider = this.getProvider();

		switch (setting.id) {
			case AppSetting.Url:
				provider.url = setting.value;
				break;
			case AppSetting.roomNamePrefix:
				provider.roomNamePrefix = setting.value;
				break;
			case AppSetting.roomNameSuffix:
				provider.roomNameSuffix = setting.value;
				break;
		}
	}

	public getProvider(): eduMEETProvider {
		if (!this.provider) {
			this.provider = new eduMEETProvider();
		}

		return this.provider;
	}
}
