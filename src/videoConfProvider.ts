/* eslint-disable @typescript-eslint/no-var-requires */
import { randomString } from '../lib/RandomString';
import type { IVideoConferenceUser } from '@rocket.chat/apps-engine/definition/videoConferences';
import type {
	IVideoConfProvider,
	IVideoConferenceOptions,
	VideoConfData,
	VideoConfDataExtended,
} from '@rocket.chat/apps-engine/definition/videoConfProviders';

export class eduMEETProvider implements IVideoConfProvider {
	public url = 'https://edumeet.geant.org';

	public roomNamePrefix = '';
	public roomNameSuffix = '';

	public name = 'eduMEET';

	public capabilities = {
		mic: false,
		cam: false,
		title: false,
	};

	public async generateUrl(call: VideoConfData): Promise<string> {
		const randstr = randomString({ length: 8 }).toLowerCase();
		call.title = call.providerData?.roomName || randstr

		const name = call.title || call._id;
		const slash = this.url.endsWith('/') ? '' : '/';

		return `${this.url}${slash}${this.roomNamePrefix}${name}${this.roomNameSuffix}`;
	}

	public async customizeUrl(call: VideoConfDataExtended, user: IVideoConferenceUser, _options: IVideoConferenceOptions): Promise<string> {
		const params: string[] = [];

		if (user) {
			params.push(`displayName=${encodeURIComponent(user.name)}`);
		}

		const query = params.join('&');
		const args = query ? `?${query}` : '';
		const url = `${call.url}${args}`;

		return url;
	}
}
