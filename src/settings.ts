import { ISetting, SettingType } from '@rocket.chat/apps-engine/definition/settings';

export enum AppSetting {
	Url = 'edumeet_url',
	roomNamePrefix = 'edumeet_room_name_prefix',
	roomNameSuffix = 'edumeet_room_name_suffix',
}

export const settings: Array<ISetting> = [
	{
		id: AppSetting.Url,
		type: SettingType.STRING,
		packageValue: '',
		required: false,
		public: true,
		i18nLabel: AppSetting.Url,
		i18nDescription: `${AppSetting.Url}_description`,
	},
	{
		id: AppSetting.roomNamePrefix,
		type: SettingType.STRING,
		packageValue: '',
		required: false,
		public: true,
		i18nLabel: AppSetting.roomNamePrefix,
		i18nDescription: `${AppSetting.roomNamePrefix}_description`,
	},
	{
		id: AppSetting.roomNameSuffix,
		type: SettingType.STRING,
		packageValue: '',
		required: false,
		public: true,
		i18nLabel: AppSetting.roomNameSuffix,
		i18nDescription: `${AppSetting.roomNameSuffix}_description`,
	},
];
